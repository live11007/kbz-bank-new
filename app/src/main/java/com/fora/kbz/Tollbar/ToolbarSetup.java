package com.fora.kbz.Tollbar;

import android.app.FragmentManager;
import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

@SuppressWarnings("deprecation")
public class ToolbarSetup implements ActionBar.TabListener {

    private Context context;
    private FragmentManager fragmentManager;
    private AppCompatActivity actionBarActivity;


    public void setup(final Context context, Toolbar toolbar, final AppCompatActivity actionBarActivity, String title, boolean upEnable, boolean searchEnable, FragmentManager fragmentManager) {

        this.context = context;
        this.fragmentManager = fragmentManager;
        this.actionBarActivity = actionBarActivity;
        actionBarActivity.setSupportActionBar(toolbar);
        actionBarActivity.getSupportActionBar().setTitle(title);

        actionBarActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);


        if (upEnable)
        {
            actionBarActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    actionBarActivity.onBackPressed();
                }
            });
        }


    }
    /*
    MANAGING TABS
     */

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    public void setIcon(int icon){
        actionBarActivity.getSupportActionBar().setIcon(icon);
    }

    public void setName(String name){
        actionBarActivity.getSupportActionBar().setTitle(name);
    }
    public  void setlogo(int logo)
    {
        actionBarActivity.getSupportActionBar().setLogo(logo);
    }
    public void hideActionbar()
    {
        actionBarActivity.getSupportActionBar().hide();
    }
}
