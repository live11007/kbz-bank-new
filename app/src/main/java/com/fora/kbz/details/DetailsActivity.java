package com.fora.kbz.details;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.blikoon.qrcodescanner.QrCodeActivity;
import com.fora.kbz.Demo;
import com.fora.kbz.QRGeneratorActivtiy;
import com.fora.kbz.R;
import com.fora.kbz.Util;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailsActivity extends AppCompatActivity {

    private static final int REQUEST_CODE_QR_SCAN = 101;
    private final String LOGTAG = "QRCScanner-MainActivity";
    @BindView(R.id.txtName)
    TextView txtName;
    @BindView(R.id.txtAddress)
    TextView txtAddress;
    @BindView(R.id.txtHubName)
    TextView txtHubName;
    @BindView(R.id.txtPhone)
    TextView txtPhone;
    @BindView(R.id.txtOrderId)
    TextView txtOrderId;
    @BindView(R.id.txtCurrency)
    TextView txtCurrency;
    @BindView(R.id.txtRequestType)
    TextView txtRequestType;
    @BindView(R.id.txtTotalAmount)
    TextView txtTotalAmount;


    @BindView(R.id.txtStatus)
    TextView txtStatus;
    @BindView(R.id.txtNoteNo1)
    TextView txtNoteNo1;
    @BindView(R.id.txtAmount1)
    TextView txtAmount1;
    @BindView(R.id.txtTotalAmount1)
    TextView txtTotalAmount1;
    @BindView(R.id.txtNoteNo2)
    TextView txtNoteNo2;
    @BindView(R.id.txtAmount2)
    TextView txtAmount2;
    @BindView(R.id.txtTotalAmount2)
    TextView txtTotalAmount2;
    @BindView(R.id.txtNoteNo3)
    TextView txtNoteNo3;
    @BindView(R.id.txtAmount3)
    TextView txtAmount3;
    @BindView(R.id.txtTotalAmount3)
    TextView txtTotalAmount3;
    @BindView(R.id.txtNoteNo4)
    TextView txtNoteNo4;
    @BindView(R.id.txtAmount4)
    TextView txtAmount4;
    @BindView(R.id.txtTotalAmount4)
    TextView txtTotalAmount4;
    @BindView(R.id.txtNoteNo5)
    TextView txtNoteNo5;
    @BindView(R.id.txtAmount5)
    TextView txtAmount5;
    @BindView(R.id.txtTotalAmount5)
    TextView txtTotalAmount5;
    @BindView(R.id.txtTotalAmountMain)
    TextView txtTotalAmountMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);

        showData();

        Util.setupToolbar(DetailsActivity.this, "Details", true);
    }

    @OnClick({R.id.btnTrack, R.id.btnScan})
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnTrack:
                startActivity(new Intent(DetailsActivity.this, Demo.class));
                break;

            case R.id.btnScan:
                Intent i = new Intent(DetailsActivity.this, QrCodeActivity.class);
                startActivityForResult(i, REQUEST_CODE_QR_SCAN);

                break;
        }

    }

    public void showData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {


            String s1 = bundle.getString("Name");
            String s2 = bundle.getString("details");
            String details = s2.replace("\n", "");
            String[] splited = s2.split("\\s*\\r?\\n\\s*");
            String hubName = splited[0];
            String phone = splited[1];

            txtPhone.setText(phone);
            txtHubName.setText(hubName);
            txtOrderId.setText(splited[2]);
            txtCurrency.setText(splited[3]);
            txtRequestType.setText(splited[4]);
            txtTotalAmount.setText(splited[5]);
            System.out.println("-------------------phone" + phone);

            String note1 = splited[6];
            String[] note1Split = note1.split(":");
            txtAmount1.setText(note1Split[1]);

            String note2 = splited[7];
            String[] note2Split = note2.split(":");
            txtAmount2.setText(note2Split[1]);


            String note3 = splited[8];
            String[] note3Split = note3.split(":");
            txtAmount3.setText(note3Split[1]);

            String note4 = splited[9];
            String[] note4Split = note4.split(":");
            txtAmount4.setText(note4Split[1]);

            String note5 = splited[10];
            String[] note5Split = note5.split(":");
            txtAmount5.setText(note5Split[1]);


            String notes1 = splited[11];
            String[] note1sSplit = notes1.split(":");
            txtNoteNo1.setText(note1sSplit[1]);

            String notes2 = splited[12];
            String[] note2sSplit = notes2.split(":");
            txtNoteNo2.setText(note2sSplit[1]);


            String notes3 = splited[13];
            String[] note3sSplit = notes3.split(":");
            txtNoteNo3.setText(note3sSplit[1]);

            String notes4 = splited[14];
            String[] note4sSplit = notes4.split(":");
            txtNoteNo4.setText(note4sSplit[1]);

            String notes5 = splited[15];
            String[] notes5Split = notes5.split(":");
            txtNoteNo5.setText(notes5Split[1]);
            txtTotalAmount1.setText("1000000");
            txtTotalAmount2.setText("2500000");
            txtTotalAmount3.setText("1000000");
            txtTotalAmount4.setText("500000");
            txtTotalAmount5.setText("0");
            String totalamount = splited[5];
            String[] totalamount5Split = totalamount.split(":");
            txtTotalAmountMain.setText(totalamount5Split[1]);

            txtName.setText(s1);
            String lat = splited[16];
            String[] lat5Split = lat.split(":");

            String lng = splited[17];
            String[] lng5Split = lng.split(":");
            LatLng sydney = new LatLng(Double.parseDouble(lat5Split[1]), Double.parseDouble(lng5Split[1]));

            String add=getAddressFromLatLng(getApplicationContext(),sydney);
            txtAddress.setText("Address: "+add);
            Log.d("Splited String ", "Splited String" + bundle.getString("details"));
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode != Activity.RESULT_OK) {
            Log.d(LOGTAG, "COULD NOT GET A GOOD RESULT.");
            if (data == null)
                return;
            //Getting the passed result
            String result = data.getStringExtra("com.blikoon.qrcodescanner.error_decoding_image");
            if (result != null) {
                AlertDialog alertDialog = new AlertDialog.Builder(DetailsActivity.this).create();
                alertDialog.setTitle("Scan Error");
                alertDialog.setMessage("QR Code could not be scanned");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
            return;

        }
        if (requestCode == REQUEST_CODE_QR_SCAN) {
            if (data == null)
                return;
            //Getting the passed result
            String result = data.getStringExtra("com.blikoon.qrcodescanner.got_qr_scan_relult");
            Log.d(LOGTAG, "Have scan result in your app activity :" + result);
            AlertDialog alertDialog = new AlertDialog.Builder(DetailsActivity.this).create();
            alertDialog.setTitle("Verify successfully.");

            alertDialog.setMessage("Your Varify Code is: " + result);
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            txtStatus.setText("Complete");
                            txtStatus.setVisibility(View.VISIBLE);

                            txtStatus.setTextColor(Color.parseColor("#049304"));
                        }
                    });
            alertDialog.show();

        }
    }

    public static String getAddressFromLatLng(Context context, LatLng latLng) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(context, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            return addresses.get(0).getAddressLine(0);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu_add,menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.nav_qr:
                Intent intent=new Intent(DetailsActivity.this, QRGeneratorActivtiy.class);
                startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
