package com.fora.kbz.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import com.fora.kbz.Dashboard;
import com.fora.kbz.Demo;
import com.fora.kbz.R;
import com.fora.kbz.Util;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Util.fullscreenView(SplashActivity.this);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {

            @SuppressWarnings("rawtypes")
            @Override
            public void run() {

                Intent i = new Intent(SplashActivity.this, Dashboard.class);
                startActivity(i);
                finish();
            }
        }, 5000);
    }
}
