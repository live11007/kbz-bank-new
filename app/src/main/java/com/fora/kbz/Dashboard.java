package com.fora.kbz;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.fora.kbz.dashboard.MarkerInfoWindowAdapter;
import com.fora.kbz.details.DetailsActivity;
import com.fora.kbz.permission.ActivityManagePermission;
import com.fora.kbz.permission.PermissionResult;
import com.fora.kbz.permission.PermissionUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class Dashboard extends ActivityManagePermission implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        Util.setupToolbar(Dashboard.this, "Dashboard", false);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        runtitmePermission();

    }

    public  void runtitmePermission()
    {

        String multile[] = {PermissionUtils.Manifest_CAMERA,PermissionUtils.Manifest_READ_EXTERNAL_STORAGE, PermissionUtils.Manifest_WRITE_EXTERNAL_STORAGE};
        askCompactPermissions(multile, new PermissionResult() {
            @Override
            public void permissionGranted() {

                // getCity(locationManager.getLatitude(),locationManager.getLongitude());
            }

            @Override
            public void permissionDenied() {
            }

            @Override
            public void permissionForeverDenied() {
            }
        });
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng sydney = new LatLng(16.82127, 96.1615363);
        mMap.addMarker(new MarkerOptions().position(sydney).snippet("Hub Name: Yangon Hub 1 \n Phone: +959426522004 \n OrderId: OID00001 \n Currency Code: MMK \n RequestType: Pick-up \n Total Amount: 5000000 \n Denom Code1: K10000 \n Denom Code2: K5000 \n Denom Code3: K1000 \n Denom Code4: K500 \n  Denom Code5: K100 \n Denom Code 1 Count: 100  \n Denom Code Count: 500  \n Denom Code 3 Count: 1000 \n Denom Code 4 Count: 1000 \n Denom Code 5 Count: 0 \n lat: 16.82127 \n lng: 96.1615363").title("Name: XYZ Mart1").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
        LatLng sydney1 = new LatLng(16.8252922, 96.1472023);
        mMap.addMarker(new MarkerOptions().position(sydney1).snippet("Hub Name: Yangon Hub 1 \n Phone: +959966939338 \n OrderId: OID00002 \n Currency Code: MMK \n RequestType: Delivery \n Total Amount: 10200000 \n Denom Code1: K10000 \n Denom Code2: K5000 \n Denom Code3: K1000 \n Denom Code4: K500 \n  Denom Code5: K100 \n Denom Code 1 Count: 300  \n Denom Code 2 Count: 800  \n Denom Code 3 Count:1850 \n Denom Code 4 Count: 700 \n Denom Code 5 Count: 10000 \n lat: 16.82127 \n lng: 96.1472023").title("Name: XYZ Mart2"));
        LatLng sydney2 = new   LatLng(16.8108892, 96.1342398);
        mMap.addMarker(new MarkerOptions().position(sydney2).snippet("Hub Name: Yangon Hub 2 \n Phone: +959426522004 \n OrderId: OID00003 \n Currency Code: MMK \n RequestType: Delivery \n Total Amount: 700000000 \n Denom Code1: K10000 \n Denom Code2: K5000 \n Denom Code3: K1000 \n Denom Code4: K500 \n  Denom Code5: K100 \n Denom Code 1 Count: 30000  \n Denom Code 2 Count: 70000  \n Denom Code 3 Count: 1000 \n Denom Code 4 Count: 97000 \n Denom Code 5 Count: 5000 \n lat: 16.82127 \n lng: 96.1472023").title("Name: XYZ Mart3"));

        LatLng sydney3 =   new LatLng(16.8071507, 96.1334995);
        mMap.addMarker(new MarkerOptions().position(sydney3).snippet("Hub Name: Yangon Hub 2 \n Phone: +959966939338 \n OrderId: OID0004 \n Currency Code: MMK \n RequestType: Delivery \n Total Amount: 50000100 \n Denom Code1: K10000 \n Denom Code2: K5000 \n Denom Code3: K1000 \n Denom Code4: K500 \n  Denom Code5: K100 \n Denom Code 1 Count: 2000  \n Denom Code 2 Count: 3500  \n Denom Code 3 Count: 7000 \n Denom Code 4 Count: 9000 \n Denom Code 5 Count: 10001 \n lat: 16.82127 \n lng: 96.1472023").title("Name: XYZ Mart4"));

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,11));
        mMap.animateCamera(CameraUpdateFactory.zoomIn());

        mMap.animateCamera(CameraUpdateFactory.zoomTo(11), 2000, null); mMap.animateCamera(CameraUpdateFactory.zoomTo(mMap.getCameraPosition().zoom - 0.5f));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney1));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney2));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney3));
  //      mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney4));

        MarkerInfoWindowAdapter markerInfoWindowAdapter = new MarkerInfoWindowAdapter(getApplicationContext());
        googleMap.setInfoWindowAdapter(markerInfoWindowAdapter);

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng arg0) {

                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(arg0);

            }
        });



        googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {

                Intent i=new Intent(Dashboard.this, DetailsActivity.class);
                i.putExtra("Name",marker.getTitle());
                i.putExtra("details",marker.getSnippet());
                startActivity(i);
            }
        });



    }

    public void onBackPressed() {


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Exit Application?");
        alertDialogBuilder
                .setMessage("Click yes to exit!")
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                moveTaskToBack(true);
                                android.os.Process.killProcess(android.os.Process.myPid());
                                System.exit(1);
                            }
                        })

                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }
}
